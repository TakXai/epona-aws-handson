data "terraform_remote_state" "staging_encryption_key" {
  backend = "s3"

  config = {
    bucket         = "tanaka-staging-terraform-tfstate"
    key            = "encryption_key/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "tanaka_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/TanakaStagingTerraformBackendAccessRole"
  }
}
