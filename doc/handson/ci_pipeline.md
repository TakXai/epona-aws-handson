# CI環境の構築手順

CI環境を整えるためには `ci_pipeline` pattern[^1]を使っていきます。

[^1]: GitLabのロゴは[GitLab](https://about.gitlab.com/)によって制作されたものであり、[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)の下に提供されています。

![ci_pipeline](../resources/ci_pipeline.jpg)

## アジェンダ

[進め方について](../../README.md#進め方について)に記載があるように、講師の指示のもと、既存コードを修正し適用していきます。

1. [ci_pipeline patternについて](#ci_pipeline-patternについて)
   1. [依存しているパターン](#依存しているパターン)
   1. [適用後の構成図](#適用後の構成図)
1. [事前準備](#事前準備)
1. [インフラ構築](#インフラ構築)
   1. [ci_pipeline pattern 修正](#ci_pipeline-pattern修正)
   1. [ci_pipeline pattern 適用](#ci_pipeline-pattern適用)
1. [アプリケーション側の設定](#アプリケーション側の設定)
   1. [GitLabのパイプライン設定について](#gitlabのパイプライン設定について)
   1. [CIで利用する変数の設定](#ciで利用する変数の設定)
   1. [動作確認](#動作確認)

## ci_pipeline patternについて

`ci_pipeline` patternとは、CIパイプラインに必要なリソースを構築してくれるものです。

[ci_pipeline pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/ci_pipeline/)

Eponaのドキュメントを用いて、以下について解説していきます。

* 概要
* 想定する適用対象環境
* 前提事項
* なぜGitLabなのか？　なぜGitLab SaaS版なのか？
* GitLab Runnerのregistration tokenについて

### 依存しているパターン

Eponaの `pattern` は、1つのパターンを適用するだけでユースケースを満たせるものばかりではありません。  
他の `pattern` を適用することが前提となっている `pattern` もあります。

`ci_pipeline pattern` は `network pattern` に依存しており、事前に適用しておく必要があります。  
依存している `network pattern`（Delivery環境のVPCの構築）については [こちら](../../README.md#vpcの構築) にて対応済みです[^1]。

[依存するpattern](https://eponas.gitlab.io/epona/guide/patterns/aws/ci_pipeline/#%E4%BE%9D%E5%AD%98%E3%81%99%E3%82%8Bpattern)

![ci_pipeline pattern 依存関係](../resources/ci_pipeline_dependencies.jpg)

### 適用後の構成図

上記環境から `ci_pipeline pattern` を適用した場合の構成図です[^1]。

![ci_pipeline pattern 適用後構成](../resources/ci_pipeline_after_apply.jpg)

---

## 事前準備

### プロジェクトを開く

プロジェクトを開きます。  
事前にforkしCloneしたディレクトリをお好みのエディタで開いてください。

* `https://gitlab.com/[gitlab account]/epona-aws-handson`
* `https://gitlab.com/[gitlab account]/handson-chat-application`

各リポジトリの用途については、以下の通りです。

|リポジトリ|用途|
|:--|:---|
|epona-aws-handson|Terraformのソースコード|
|handson-chat-application|AWSに載せるアプリケーション|

### 利用するユーザー情報の確認

#### アカウント情報の用意

事前にAWSアカウント情報をお渡ししています。以下AWSアカウント情報を用意してください。

```text
[Delivery環境]
  …
  [Terraform実行ユーザー]
    ユーザー名                   ：[last_name]DeliveryTerraformer
    AWSアクセスキー             ：aws_access_key_id
    AWSシークレットアクセスキー   ：aws_secret_access_key
    リージョン                 ：aws_region
```

#### 実行ユーザーの設定

利用するユーザーの情報を設定します。  
お好みのコマンドラインツールを開いて、以下サイトを参考に設定してください。

[AWS CLI を設定する環境変数](https://docs.aws.amazon.com/ja_jp/cli/latest/userguide/cli-configure-envvars.html)

上記設定が無事成功しているかを確認していきます。  
`$ aws sts get-caller-identity --query Arn --output text` を実行してください。

以下のように出力されたら成功です。  
※ `[LastName]` は事前に指定されたユーザー名の値が出力されていること。

```shell script
$ aws sts get-caller-identity --query Arn --output text
arn:aws:iam::938285887320:user/[LastName]DeliveryTerraformer
```

全て実施した方は、Zoomの「手を挙げる」機能を使って講師へお知らせください。  
（全員手が挙がっていることが確認でき次第、次の説明へ移ります）

### Terraform バージョン確認

Terraformのバージョンを確認します。

```shell script
$ terraform --version
```

バージョンは `0.13.5` であれば問題ありません。

```shell script
$ terraform --version
Terraform v0.13.5
```

### AWSコンソールへログイン

AWSコンソールへログインしてください。

[AWSコンソールログインについて](./aws_console_login.md)

---

## インフラ構築

GitLab CI/CDで利用するためのGitLab RunnerをAWS環境に構築します。

### ゴール

* TerraformでGitLab RunnerがインストールされたAmazon EC2を構築する
* TerraformでアプリケーションをデプロイするためのAmazon ECRとAmazon S3を構築する
* 上記をAWSコンソールから確認する

---

### ci_pipeline pattern修正

ci_pipelineパターンのディレクトリに移動します。  
※[last_name] については、事前に指定されたユーザー名のディレクトリを利用すること。

```shell script
$ cd epona-aws-handson/[last_name]/delivery/ci_pipeline
```

#### ディレクトリ構成について

`ci_pipeline` を例に、Eponaにおける[ディレクトリ構成](https://eponas.gitlab.io/epona/guide/patterns/#ディレクトリ構成)について説明します。

Eponaでは、以下のようなディレクトリ構成を想定しています。  
:warning: なお、Terraformによる制約ではありません。

```text
├── delivery
│   ├── [インスタンス名]  // 例：ci_pipeine
│   │         ├── main.tf
│   │         └── ...
│   └── runtime_instances
│             └── [Runtime環境名]  // 例：staging
│                    └── [インスタンス名]  // 例：cd_pipeline_backend_trigger_backend
│                            ├── main.tf
│                            └── ...
└── runtimes
      └── [Runtime環境名]  // 例：staging
             └── [インスタンス名]  // 例：cacheable_frontend
                       ├── main.tf
                       └── ...
```

|ディレクトリ名|目的|
|:--|:--|
|delivery|Delivery環境用のインスタンス定義を収めたディレクトリ|
|delivery/runtime_instances|Runtime環境の構築に応じて、Delivery環境へ変更を加えるためのインスタンス定義|
|runtimes|Runtime環境用のインスタンス定義を収めたディレクトリ|

また、今回のハンズオンでは以下のようなモジュール構成としています。

```text
└── ci_pipeline
         ├── main.tf
         ├── instance_dependencies.tf
         ├── outputs.tf
         ├── variables.tf
         └── versions.tf
```

|ファイル名|目的|
|:--|:--|
|main.tf|主要なファイルとして利用する|
|instance_dependencies.tf|外部データの参照先を定義する|
|outputs.tf|実行後の戻り値の出力内容を定義する|
|variables.tf|変数を定義する|
|versions.tf|Terraformなどのバージョンを指定したり、 `terraform.tfstate` の配置を定義する|

ハンズオン終了後により理解を深めたい場合、Terraformより [Standard Module Structure](https://www.terraform.io/docs/language/modules/develop/structure.html)が紹介されていますのでご活用ください。

#### TODOコメント対応

では、講師の指示に従って、ci_pipelineディレクトリ配下のTODOコメントに対応してください。

#### ワークスペースの初期化

Terraformのワークスペースを初期化します。

```shell script
$ terraform init
```

以下のような表示が出ればOKです。

```shell script
$ terraform init
…
Terraform has been successfully initialized!
…
```

---

### ci_pipeline pattern適用

ここでは、修正したソースコードを使ってリソースを作成していきます。

#### terraform plan

編集したterraformコードの実行計画を確認してみましょう。  
ci_pipelineのディレクトリで、以下コマンドを実行してください。

```shell script
$ terraform plan
```

すると、以下のように入力待ちの状態となります。 `1` と入力し、Enterを押してください。  
ここでは本来、GitLab Runner登録のためのTokenを入力する必要があります。  
しかし `terraform plan` ではAWSリソースの変更を反映せず、ここではリソースの変更内容を確認したいため、上記対応をとっています。

```shell script
$ terraform plan
var.gitlab_runner_registration_token
  GitLab Runner登録のためのトークン（cloud-initスクリプトを独自定義する場合は、この変数は不要になる可能性がある）
  Enter a value:
```

次に、作成されるリソース（実行計画）を確認します。

* `[last_name]` の修正が反映されていること
* `Plan: 16 to add, 0 to change, 0 to destroy` という出力があること

```shell script
$ terraform plan
…
   + “Name”   = [last_name]-gitlab-runner
…

Plan: 16 to add, 0 to change, 0 to destroy

…
```

#### terraform apply

変更を反映させるため、`terraform apply` します。

以下コマンドを入力してください。

```shell script
$ terraform apply
```

すると、再び以下のように入力待ちの状態となります。ここでGitLab Runner登録のためのTokenを取得していきましょう。

```shell script
$ terraform apply
var.gitlab_runner_registration_token
  GitLab Runner登録のためのトークン（cloud-initスクリプトを独自定義する場合は、この変数は不要になる可能性がある）
  Enter a value:
```

1. Forkした「handson-chat-application」のGitLabリポジトリをブラウザで開きます
   1. :warning: handson-chat-applicationのリポジトリは、フォークしたリポジトリを参照しているか確認してください
1. `Settings` > `CI/CD` > `Runners` > `Set up a specific Runner manually` を選択します
1. 3.Use the … の値をコピーし、コマンドラインにペーストしてください
1. Enterを押下してください

Enter押下後に、再度入力待ちの状態となります。変更を反映してよいか聞かれていればOKです。  
※ `yes` はまだ入力しないでください。

```shell script
$ terraform apply
…Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value:
```

反映させる内容を確認してください。

* `[last_name]` の修正が反映されていること
* `Plan: 16 to add, 0 to change, 0 to destroy` という出力があること

```shell script
$ terraform apply
…
   + “Name”   = [last_name]-gitlab-runner
…

Plan: 16 to add, 0 to change, 0 to destroy

…
```

問題なければ `yes` と入力し、Enterを押下してください。

* `Apply complete! Resources: 16 added, 0 changed, 0 destroyed.` という出力があること
* `Outputs` に `ci_pipeline` についての出力がされていること

```shell script
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value: yes

…
Apply complete! Resources: 16 added, 0 changed, 0 destroyed.

Outputs:
ci_pipeline = {
  …
}
```

#### コンソール上から確認

リソースが無事作成されていることを確認してみましょう。講師の指示に従ってください。

[AWSコンソール](https://aws.amazon.com/jp/console/)

* Amazon S3
* Amazon ECR
* Amazon EC2

---

## アプリケーション側の設定

AWS上にGitLab RunnerとなるAmazon EC2とビルド結果を格納するためのAmazon ECR, Amazon S3を作成しました。  
このあとは、GitLab CI/CDの設定をしていきます（デプロイ設定）。

### ゴール

* GitLabに環境変数の設定と既存コードを修正してGitLab CI/CDを実施する
* GitLab CI/CDによって、Amazon ECRとAmazon S3へデプロイを実施する
* AWSコンソールからデプロイされたリソースを閲覧する

### GitLabのパイプライン設定について

GitLabでは、 `.gitlab-ci.yml` というファイルを使ってパイプラインの定義することが出来ます。  
ハンズオンで利用するアプリケーションのCIパイプラインは以下に定義されています。

[handson-chat-application/.gitlab-ci.yml](https://gitlab.com/eponas/epona-handson/handson-chat-application/-/blob/master/.gitlab-ci.yml)

ハンズオンで利用する `.gitlab-ci.yml` では、以下のようにシェルスクリプトを読み込んでいます。

```yaml
...

# GitLab container resistoryに登録されたバックエンドアプリケーションのコンテナイメージをECRにデプロイするジョブ
deploy-backend-to-ecr:
  extends: .deploy-aws
  variables:
    IMAGE_PUSHED_TO_ECR: $CI_REGISTRY_IMAGE/example-chat-backend:$CI_COMMIT_REF_NAME
    ECR_REPOSITORY: $EPONA_ECR_BASE_URI/$EPONA_ECR_BACKEND_REPOSITORY_NAME
  script:
    - .ci/deploy-to-ecr.sh

...
```

`deploy-to-ecr.sh` を確認すると、多くの変数が定義/利用されていることがわかります。

[deploy-to-ecr.sh](https://gitlab.com/eponas/epona-handson/handson-chat-application/-/blob/master/.ci/deploy-to-ecr.sh)

```shell script
#!/bin/sh
# GitLab Container Repository 上の Image ($IMAGE_PUSHED_TO_ECR) にタグ付けした上で ECR に PUSH する

set -eux

# GitLab Container Registry から、branch に対応する Image を Pull する
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker image pull $IMAGE_PUSHED_TO_ECR

# ECR 用のタグ付け
docker image tag $IMAGE_PUSHED_TO_ECR $ECR_REPOSITORY:$CI_COMMIT_REF_NAME
docker image tag $IMAGE_PUSHED_TO_ECR $ECR_REPOSITORY:$CI_COMMIT_SHORT_SHA

# master branch への merge の場合は latest タグを付与する
if [ "$CI_COMMIT_REF_NAME" == "master" ]; then
    docker image tag $IMAGE_PUSHED_TO_ECR $ECR_REPOSITORY:latest
fi
aws ecr get-login-password | docker login --username AWS --password-stdin $ECR_REPOSITORY
docker image push $ECR_REPOSITORY

```

つまり、このような変数をGitLabに登録してあげる必要があります。

### CIで利用する変数の設定

ブラウザより、`handson-chat-application` を開き値を設定してください。  
`Forkしたリポジトリ` > `Settings` > `CI/CD` > `Variables` > `Add Value`

:warning: handson-chat-applicationのリポジトリは、フォークしたリポジトリを参照しているか確認してください。

なお、すべての値に置いて `Flags` の `Protected` `Mask` のチェックボックスは外してください。

|Key                                     |Value                                                               |
|:---------------------------------------|:-------------------------------------------------------------------|
|`EPONA_AWS_DEFAULT_REGION`              |`ap-northeast-1`                                                    |
|`EPONA_ECR_BASE_URI`                    |`938285887320.dkr.ecr.ap-northeast-1.amazonaws.com`                 |
|`EPONA_ECR_BACKEND_REPOSITORY_NAME`     |`[last_name]-chat-example-backend`                                  |
|`EPONA_ECR_NOTIFIER_REPOSITORY_NAME`    |`[last_name]-chat-example-ntfr`                                 |
|`EPONA_FRONTEND_PUSH_TO_S3_BUCKET_NAME` |`[last_name]-static-resource`                                       |
|`EPONA_FRONTEND_ARCHIVE_FILE_NAME`      |`source.zip`                                                        |
|`EPONA_REACT_APP_BACKEND_BASE_URL`      |`https://[last_name]-chat-example-backend.staging.epona-handson.com`|

設定後は、以下の図[^2]のようになります。

[^2]: `handson-chat-application` [CI/CD設定](https://gitlab.com/eponas/epona-handson/handson-chat-application/-/settings/ci_cd)より、2020年11月20日19時にスクリーンショットを取得。

![GitLab Runner Variables](../resources/gitrub-runner-variables.jpg)

---

### 動作確認

#### テストの修正

試しに `handson-chat-application` のmasterブランチに変更を加えてみましょう。  
現在の状況では、テストが落ちるようになっています。

以下のファイルを開いて、TODOコメントにしたがって修正してください。

`backend/src/test/java/com/example/domain/model/message/MessageIdTest.java`

なお、修正後はmasterブランチに直接コミット, PUSHしてください。

#### パイプラインが動き始めたことを確認

`Forkしたリポジトリ` > `CI/CD` > `Pipeline` を開いてください。  
パイプラインが動いていることが確認できます。

#### AWSコンソール上から確認

パイプラインの全ジョブにチェックマークがついたら、ci_pipeline patternで構築したリソースにファイルが配置されているはずです。

AWSコンソールから、Amazon S3、Amazon ECRを確認してみましょう。  
コンテナイメージやzipファイルがアップロードされていればOKです。
