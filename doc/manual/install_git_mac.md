# Gitのインストール(Mac)

## Gitのインストール

brewを使用してGitをインストールします。  
以下を実行してください。

```shell script
brew install git
```

処理が終了した時に :beer: のアイコンが出ていたらOKです。

## 確認

以下を実行してください。 `git` コマンドが動くことを確認できればOKです。

```shell script
$ git --version
git version 2.29.2
```

なお、versionは2020/11/25現在の最新2.29.2で記載していますが、その時点の最新で構いません。
